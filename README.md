# Setting Up QEMU and Virt-Manager on Debian-Based Linux

This guide provides step-by-step instructions on installing and configuring QEMU and Virt-Manager on Debian-based Linux distributions.

## Prerequisites

- A Debian-based Linux distribution (e.g., Ubuntu, Debian)
- Sudo or root access
- Internet connection

## Check if your host machine is capable of running virtual machines.

Checking whether your host machine is capable of running virtual machines, particularly with hardware acceleration, is an important step. 

Here are the key commands to run on a Linux host to check for virtualization support:

### 1. Check for CPU Virtualization Support:

For Intel/AMD CPUs, use:

```sh
grep -E 'vmx|svm' /proc/cpuinfo
```

If this command returns results (like a list of flags), it means your CPU supports hardware virtualization, which is essential for efficient VM operation.

### 2. Check if KVM Kernel Modules are Loaded:

```sh
lsmod | grep kvm
```

This command checks if the necessary KVM (Kernel-based Virtual Machine) modules are loaded in your Linux kernel. If you see `kvm_intel` or `kvm_amd` in the output, it means KVM is active.

## Installation

### Step 1: Update System Packages

Before installing new packages, it's a good practice to update your system's package index.

```sh
sudo apt update
sudo apt upgrade -y
```

### Step 2: Install QEMU and Virt-Manager

QEMU is an open-source machine emulator and virtualizer, and Virt-Manager is a graphical tool for managing virtual machines. Install them using the following command:

```sh
sudo apt install qemu qemu-kvm libvirt-daemon libvirt-clients bridge-utils virt-manager -y
```

### Step 3: Add User to Relevant Groups

To manage virtual machines without root privileges, add your user to the libvirt and kvm groups:

```sh
sudo usermod -aG libvirt $(whoami)
sudo usermod -aG kvm $(whoami)
```

Log out and log back in for the group changes to take effect.

### Step 4: Verify Installation

Check if the libvirt daemon is running:

```sh
sudo systemctl status libvirtd
```

## Conclusion

You now have QEMU and Virt-Manager installed on your Debian-based system, ready for creating and managing virtual machines.

